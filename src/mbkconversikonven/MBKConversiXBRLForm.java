// 
// Decompiled by Procyon v0.5.36
// 

package mbkconversikonven;

import javax.xml.stream.XMLStreamException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import java.io.IOException;
import javax.xml.stream.XMLStreamReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.openxml4j.opc.OPCPackage;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import view.MainView;
import javax.swing.UIManager;
import java.awt.Component;
import javax.swing.JOptionPane;
import java.util.Date;
import java.text.SimpleDateFormat;

public class MBKConversiXBRLForm
{
    public static void main(final String[] args) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        try {
//            final Date dateStart = sdf.parse("2020-12-12");
//            if (new Date().after(dateStart)) {
//                JOptionPane.showMessageDialog(null, "Please purchase this source to wa:081287482485 an Ikhsan Fadly", "Informasi", 1);
//                return;
//            }
            final long heapSize = Runtime.getRuntime().totalMemory();
            System.out.println("Heap Size: " + heapSize);
            try {
                UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
            }
            catch (Exception ex2) {}
            new MainView().setVisible(true);
//        }
//        catch (ParseException ex) {
//            Logger.getLogger(MBKConversiXBRLForm.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
    
    public static void testStreamBigFile() throws IOException, OpenXML4JException, XMLStreamException {
        final String fileName = "C:\\Users\\getri\\Downloads\\Book1.xlsx";
        final String ROW = "row";
        final String CELL = "c";
        final String CELL_TYPE = "t";
        final String CELL_TYPE_STRING = "s";
        final OPCPackage pkg = OPCPackage.open("C:\\Users\\getri\\Downloads\\Book1.xlsx");
        final XSSFReader r = new XSSFReader(pkg);
        final SharedStringsTable sst = r.getSharedStringsTable();
        final InputStream sheet2 = r.getSheetsData().next();
        final XMLInputFactory fac = XMLInputFactory.newInstance();
        final XMLStreamReader parser = fac.createXMLStreamReader(sheet2);
        String cellType = "";
        while (parser.hasNext()) {
            final int eventType = parser.next();
            if (eventType == 1) {
                if (parser.getLocalName().equals("row")) {
                    System.out.println();
                    System.out.print("row : " + parser.getAttributeValue(0));
                }
                else {
                    if (!parser.getLocalName().equals("c")) {
                        continue;
                    }
                    System.out.print(" | " + parser.getAttributeValue(0));
                    cellType = parser.getAttributeValue(2);
                }
            }
            else {
                if (!(eventType == 12 | eventType == 4)) {
                    continue;
                }
                if (cellType != null && cellType.equals("s")) {
                    final String value = parser.getText();
                    try {
                        System.out.print(" = " + new XSSFRichTextString(sst.getEntryAt(Integer.parseInt(value))));
                    }
                    catch (NumberFormatException ex) {
                        System.out.print(" = " + value);
                    }
                }
                else {
                    System.out.print(" = " + parser.getText());
                }
            }
        }
        parser.close();
        sheet2.close();
        pkg.close();
    }
}
