// 
// Decompiled by Procyon v0.5.36
// 

package view;

import java.awt.EventQueue;
import javax.swing.UnsupportedLookAndFeelException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.LayoutStyle;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.GroupLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import util.Constant;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JProgressBar;
import javax.swing.DefaultListModel;
import controller.MainViewController;
import javax.swing.JFrame;

public class MainView extends JFrame
{
    private final MainViewController mainViewController;
    private final DefaultListModel<String> listModel;
    private JProgressBar progressBar;
    private JComboBox<String> cmbSheet;
    private JButton jButton1;
    private JButton jButton2;
    private JButton jButton3;
    private JButton jButton4;
    private JLabel jLabel1;
    private JLabel jLabel14;
    private JLabel jLabel15;
    private JLabel jLabel16;
    private JLabel jLabel17;
    private JLabel jLabel18;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JScrollPane jScrollPane1;
    private JLabel labelError;
    private JLabel labelNamaFile;
    private JLabel labelNotif;
    private JLabel labelProccessed;
    private JLabel labelTime;
    private JLabel labelTotalRow;
    private JPanel rootPanel;
    private JTabbedPane tabbedPane;
    private JTextField txtFileLocation;
    private JTextField txtFolderSaveLocation;
    private JTextField txtKodePerusahaan;
    private JTextArea txtLog;
    private JTextField txtNamaPerusahaan;
    private JTextField txtPeriodePelaporan;
    private JTextField txtWaktuPembacaanFile;
    
    public MainView() {
        this.listModel = new DefaultListModel<String>();
        this.initComponents();
        this.mainViewController = new MainViewController(this.labelNotif);
        this.cmbSheet.removeAllItems();
        this.cmbSheet.addItem(":: Sheet Name ::");
        for (final String sheet : Constant.listSheetNameAvailible) {
            this.cmbSheet.addItem(sheet);
        }
    }
    
    private void initComponents() {
        this.rootPanel = new JPanel();
        this.tabbedPane = new JTabbedPane();
        this.jPanel1 = new JPanel();
        this.jLabel1 = new JLabel();
        this.cmbSheet = new JComboBox<String>();
        this.jLabel2 = new JLabel();
        this.jLabel3 = new JLabel();
        this.txtFileLocation = new JTextField();
        this.txtFolderSaveLocation = new JTextField();
        this.jButton1 = new JButton();
        this.jButton2 = new JButton();
        this.jLabel4 = new JLabel();
        this.txtPeriodePelaporan = new JTextField();
        this.jButton3 = new JButton();
        this.jButton4 = new JButton();
        this.labelNamaFile = new JLabel();
        this.txtNamaPerusahaan = new JTextField();
        this.jLabel16 = new JLabel();
        this.jLabel17 = new JLabel();
        this.txtKodePerusahaan = new JTextField();
        this.labelNotif = new JLabel();
        this.jLabel5 = new JLabel();
        this.txtWaktuPembacaanFile = new JTextField();
        this.jPanel2 = new JPanel();
        this.jLabel6 = new JLabel();
        this.jLabel7 = new JLabel();
        this.jLabel8 = new JLabel();
        this.labelTotalRow = new JLabel();
        this.labelProccessed = new JLabel();
        this.labelError = new JLabel();
        this.jLabel18 = new JLabel();
        this.labelTime = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.txtLog = new JTextArea();
        this.jLabel14 = new JLabel();
        this.jLabel15 = new JLabel();
        this.setDefaultCloseOperation(3);
        this.setTitle("Aplikasi Conversi XBRL Form");
        this.setResizable(false);
        this.rootPanel.setBackground(new Color(255, 255, 255));
        this.rootPanel.setToolTipText("");
        this.jPanel1.setBackground(new Color(255, 255, 255));
        this.jLabel1.setText("Sheet :");
        this.cmbSheet.setModel(new DefaultComboBoxModel<String>(new String[] { "::Sheet::", "214021300", "214021350" }));
        this.jLabel2.setText("Lokasi Penyimpanan Instance :");
        this.jLabel3.setText("File :");
        this.txtFileLocation.setEditable(false);
        this.txtFolderSaveLocation.setEditable(false);
        this.jButton1.setText("...");
        this.jButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                MainView.this.jButton1ActionPerformed(evt);
            }
        });
        this.jButton2.setText("...");
        this.jButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                MainView.this.jButton2ActionPerformed(evt);
            }
        });
        this.jLabel4.setText("Periode Pelaporan :");
        this.txtPeriodePelaporan.setEditable(false);
        this.jButton3.setText("Start");
        this.jButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                MainView.this.jButton3ActionPerformed(evt);
            }
        });
        this.jButton4.setText("Close");
        this.jButton4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                MainView.this.jButton4ActionPerformed(evt);
            }
        });
        this.labelNamaFile.setFont(new Font("Tahoma", 1, 14));
        this.labelNamaFile.setHorizontalAlignment(0);
        this.labelNamaFile.setText(":: Nama File ::");
        this.txtNamaPerusahaan.setEditable(false);
        this.jLabel16.setText("Nama Perusahaan :");
        this.jLabel17.setText("Kode Perusahaan :");
        this.txtKodePerusahaan.setEditable(false);
        this.labelNotif.setFont(new Font("Tahoma", 1, 12));
        this.labelNotif.setForeground(new Color(255, 0, 0));
        this.jLabel5.setText("Waktu Pembacaan File :");
        this.txtWaktuPembacaanFile.setEditable(false);
        final GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup().addContainerGap(-1, 32767).addComponent(this.jButton3).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jButton4).addGap(15, 15, 15)).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.labelNamaFile, -1, -1, 32767).addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.labelNotif, -1, -1, 32767).addGroup(jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel16, -1, -1, 32767).addComponent(this.jLabel17, -1, -1, 32767)).addGap(5, 5, 5).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.txtNamaPerusahaan, -1, 283, 32767).addComponent(this.txtKodePerusahaan))).addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false).addComponent(this.jLabel4, -1, -1, 32767).addComponent(this.jLabel2, GroupLayout.Alignment.LEADING, -1, -1, 32767).addComponent(this.jLabel1, GroupLayout.Alignment.LEADING, -1, -1, 32767).addComponent(this.jLabel3, GroupLayout.Alignment.LEADING, -1, -1, 32767)).addComponent(this.jLabel5)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.txtPeriodePelaporan, GroupLayout.Alignment.TRAILING).addComponent(this.txtFileLocation).addComponent(this.txtFolderSaveLocation, GroupLayout.Alignment.TRAILING).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.cmbSheet, -2, 130, -2).addGap(0, 153, 32767)).addComponent(this.txtWaktuPembacaanFile)))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jButton1, -2, 28, -2).addComponent(this.jButton2, -2, 28, -2)))).addGap(7, 7, 7))).addContainerGap()));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addGap(20, 20, 20).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel17).addComponent(this.txtKodePerusahaan, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel16).addComponent(this.txtNamaPerusahaan, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel4).addComponent(this.txtPeriodePelaporan, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel3).addComponent(this.txtFileLocation, -2, -1, -2).addComponent(this.jButton1)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.txtWaktuPembacaanFile, -2, -1, -2).addComponent(this.jLabel5, -2, 14, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.txtFolderSaveLocation, -2, -1, -2).addComponent(this.jButton2).addComponent(this.jLabel2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this.cmbSheet, -2, -1, -2)).addGap(34, 34, 34).addComponent(this.labelNotif, -2, 14, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.labelNamaFile).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 11, 32767).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jButton3).addComponent(this.jButton4)).addContainerGap()));
        this.tabbedPane.addTab("Conversi", this.jPanel1);
        this.jPanel2.setBackground(new Color(255, 255, 255));
        this.jLabel6.setText("Row :");
        this.jLabel7.setText("Proccessed :");
        this.jLabel8.setText("Error :");
        this.labelTotalRow.setText("0");
        this.labelProccessed.setText("0");
        this.labelError.setText("0");
        this.jLabel18.setText("Time :");
        this.labelTime.setText("N/A");
        this.txtLog.setColumns(20);
        this.txtLog.setRows(5);
        this.jScrollPane1.setViewportView(this.txtLog);
        final GroupLayout jPanel2Layout = new GroupLayout(this.jPanel2);
        this.jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1, -1, 482, 32767).addGroup(jPanel2Layout.createSequentialGroup().addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.jLabel18, -1, -1, 32767).addComponent(this.jLabel8, -1, -1, 32767).addComponent(this.jLabel7, -1, 88, 32767).addComponent(this.jLabel6, -1, -1, 32767)).addGap(18, 18, 18).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.labelTime, -1, -1, 32767).addComponent(this.labelTotalRow, -1, -1, 32767).addComponent(this.labelError, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addComponent(this.labelProccessed, -1, -1, 32767)))).addContainerGap()));
        jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup().addGap(20, 20, 20).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel6).addComponent(this.labelTotalRow)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel7).addComponent(this.labelProccessed)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel8).addComponent(this.labelError)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel18).addComponent(this.labelTime)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane1, -1, 207, 32767).addContainerGap()));
        this.tabbedPane.addTab("Message Log", this.jPanel2);
        this.jLabel14.setFont(new Font("Tahoma", 1, 11));
        this.jLabel14.setText("Konversi Syariah(.xlsm) to XML");
        this.jLabel15.setFont(new Font("Tahoma", 1, 11));
        this.jLabel15.setText("MBK");
        final GroupLayout rootPanelLayout = new GroupLayout(this.rootPanel);
        this.rootPanel.setLayout(rootPanelLayout);
        rootPanelLayout.setHorizontalGroup(rootPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(rootPanelLayout.createSequentialGroup().addContainerGap().addGroup(rootPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.tabbedPane).addGroup(rootPanelLayout.createSequentialGroup().addComponent(this.jLabel14, -2, 243, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.jLabel15))).addContainerGap()));
        rootPanelLayout.setVerticalGroup(rootPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(rootPanelLayout.createSequentialGroup().addContainerGap().addGroup(rootPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel14).addComponent(this.jLabel15)).addGap(4, 4, 4).addComponent(this.tabbedPane).addContainerGap()));
        final GroupLayout layout = new GroupLayout(this.getContentPane());
        this.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.rootPanel, -1, -1, 32767));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.rootPanel, -1, -1, 32767));
        this.pack();
    }
    
    private void jButton1ActionPerformed(final ActionEvent evt) {
        this.mainViewController.openFileV2(this, this.txtFileLocation, this.labelNamaFile, this.txtKodePerusahaan, this.txtNamaPerusahaan, this.txtPeriodePelaporan, this.txtFolderSaveLocation, this.txtWaktuPembacaanFile);
    }
    
    private void jButton2ActionPerformed(final ActionEvent evt) {
        this.mainViewController.openFolderSave(this, this.txtFolderSaveLocation);
    }
    
    private void jButton4ActionPerformed(final ActionEvent evt) {
        System.exit(0);
    }
    
    private void jButton3ActionPerformed(final ActionEvent evt) {
        this.mainViewController.start214021300(this, this.tabbedPane, this.labelError, this.labelProccessed, this.labelTime, this.labelTotalRow, this.progressBar, this.cmbSheet, this.txtFileLocation, this.labelNamaFile, this.txtKodePerusahaan, this.txtNamaPerusahaan, this.txtPeriodePelaporan, this.txtFolderSaveLocation, this.txtLog);
    }
    
    public static void main(final String[] args) {
        try {
            for (final UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(MainView.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex2) {
            Logger.getLogger(MainView.class.getName()).log(Level.SEVERE, null, ex2);
        }
        catch (IllegalAccessException ex3) {
            Logger.getLogger(MainView.class.getName()).log(Level.SEVERE, null, ex3);
        }
        catch (UnsupportedLookAndFeelException ex4) {
            Logger.getLogger(MainView.class.getName()).log(Level.SEVERE, null, ex4);
        }
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainView().setVisible(true);
            }
        });
    }
}
