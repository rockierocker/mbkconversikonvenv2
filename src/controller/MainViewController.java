// 
// Decompiled by Procyon v0.5.36
// 

package controller;

import java.awt.Frame;
import view.DialogValidateS214021300;
import util.Constant;
import java.text.ParseException;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.text.MessageFormat;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import java.io.FileNotFoundException;
import model.sheet.S214021300_TUP;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;
import javax.xml.stream.XMLStreamReader;
import java.util.Iterator;
import org.apache.poi.xssf.model.SharedStringsTable;
import javax.xml.stream.XMLStreamException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import java.io.IOException;
import javax.swing.JOptionPane;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import javax.xml.stream.XMLInputFactory;
import util.SpreadSheetUtil;
import java.io.InputStream;
import org.apache.poi.openxml4j.opc.OPCPackage;
import java.io.FileInputStream;
import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;
import java.awt.Component;
import javax.swing.JTextField;
import javax.swing.JFrame;
import javax.swing.filechooser.FileFilter;
import view.ExtensionFileFilter;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import javax.swing.JLabel;
import java.text.SimpleDateFormat;
import java.io.File;
import javax.swing.JFileChooser;

public class MainViewController
{
    private JFileChooser jfc;
    private JFileChooser jfcFolder;
    private File fileExcell;
    private File folderSave;
    private SimpleDateFormat sdf;
    private JLabel labelNotif;
    private XSSFWorkbook workbook;
    Boolean endThreadOpenFile;
    Boolean endThreadStart;
    private XSSFReader globalR;
    
    public MainViewController(final JLabel labelNotif) {
        this.sdf = new SimpleDateFormat("yyyy-MM-dd");
        this.endThreadOpenFile = false;
        this.endThreadStart = true;
        this.labelNotif = labelNotif;
        (this.jfc = new JFileChooser()).setAcceptAllFileFilterUsed(false);
        final FileFilter filter = new ExtensionFileFilter("doc .xlsx/.xlsm", new String[] { "XLSX", "XLSM" });
        this.jfc.setFileFilter(filter);
        (this.jfcFolder = new JFileChooser()).setAcceptAllFileFilterUsed(false);
        this.jfcFolder.setFileSelectionMode(1);
        final FileFilter filterFolder = new FileFilter() {
            @Override
            public boolean accept(final File f) {
                return f.isDirectory();
            }
            
            @Override
            public String getDescription() {
                return "Directory Only";
            }
        };
        this.jfcFolder.setFileFilter(filterFolder);
    }
    
    public void openFileV2(final JFrame parent, final JTextField txtFileLocation, final JLabel labelNamaFile, final JTextField txtKodePerusahaan, final JTextField txtNamaPerusahaan, final JTextField txtPeriodePelaporan, final JTextField txtFolderSaveLocation, final JTextField txtWaktuPembacaanFile) {
        this.labelNotif.setText("");
        if (0 == this.jfc.showOpenDialog(parent)) {
            txtFileLocation.setText(this.jfc.getSelectedFile().getPath());
            labelNamaFile.setText(":: " + this.jfc.getSelectedFile().getName() + " ::");
            txtWaktuPembacaanFile.setText(new SimpleDateFormat("dd MMM yyyy, HH:mm:ss").format(new Date()));
            txtKodePerusahaan.setText("");
            txtNamaPerusahaan.setText("");
            txtPeriodePelaporan.setText("");
            txtFolderSaveLocation.setText("");
            this.endThreadOpenFile = false;
            final Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    int i = 0;
                    String text = "Opening File";
                    while (!MainViewController.this.endThreadOpenFile) {
                        text = "Opening File";
                        for (int j = 0; j < i; ++j) {
                            text += ".";
                        }
                        if (++i == 7) {
                            i = 0;
                        }
                        try {
                            Thread.sleep(300L);
                        }
                        catch (InterruptedException ex) {
                            Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        MainViewController.this.labelNotif.setForeground(Color.blue);
                        MainViewController.this.labelNotif.setText(text);
                    }
                    if (MainViewController.this.endThreadOpenFile) {
                        MainViewController.this.labelNotif.setText("");
                    }
                }
            });
            final Thread tProccess = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        final OPCPackage pkg = OPCPackage.open((InputStream)new FileInputStream(MainViewController.this.jfc.getSelectedFile().getPath()));
                        final XSSFReader r = new XSSFReader(pkg);
                        final SharedStringsTable sst = r.getSharedStringsTable();
                        final Iterator<InputStream> sheets = (Iterator<InputStream>)r.getSheetsData();
                        MainViewController.this.globalR = r;
                        boolean found = false;
                        if (sheets instanceof XSSFReader.SheetIterator) {
                            final XSSFReader.SheetIterator sheetiterator = (XSSFReader.SheetIterator)sheets;
                            while (sheetiterator.hasNext()) {
                                final InputStream isSheet = sheetiterator.next();
                                System.out.println(sheetiterator.getSheetName());
                                if (sheetiterator.getSheetName().trim().toLowerCase().equals(SpreadSheetUtil.UUS_SHEET_MENU_UTAMA.sheetName.toLowerCase())) {
                                    found = true;
                                    final XMLInputFactory fac = XMLInputFactory.newInstance();
                                    final XMLStreamReader parser = fac.createXMLStreamReader(isSheet);
                                    String cellType = "";
                                    String currentCell = "";
                                    final Integer valueIsSetEndAt = 4;
                                    Integer currentValueIsSet = 0;
                                    while (parser.hasNext()) {
                                        final int eventType = parser.next();
                                        if (eventType == 1) {
                                            if (parser.getLocalName().equals("row")) {
                                                System.out.println();
                                                System.out.print("row : " + parser.getAttributeValue(0));
                                            }
                                            else {
                                                if (!parser.getLocalName().equals("c")) {
                                                    continue;
                                                }
                                                currentCell = parser.getAttributeValue(0);
                                                System.out.print(" | " + currentCell);
                                                cellType = parser.getAttributeValue(2);
                                            }
                                        }
                                        else {
                                            if (!(eventType == 12 | eventType == 4)) {
                                                continue;
                                            }
                                            String value = "";
                                            if (cellType != null && cellType.equals("s")) {
                                                value = parser.getText();
                                                final Integer intValue = null;
                                                try {
                                                    System.out.print(" = " + new XSSFRichTextString(sst.getEntryAt(Integer.parseInt(value))));
                                                    value = String.valueOf(new XSSFRichTextString(sst.getEntryAt(Integer.parseInt(value))));
                                                }
                                                catch (NumberFormatException ex4) {
                                                    System.out.print(" = " + value);
                                                }
                                            }
                                            else {
                                                System.out.print(" = " + parser.getText());
                                                value = parser.getText();
                                            }
                                            if ("C3".equals(currentCell)) {
                                                ++currentValueIsSet;
                                                txtKodePerusahaan.setText(value);
                                            }
                                            if ("C4".equals(currentCell)) {
                                                ++currentValueIsSet;
                                                txtNamaPerusahaan.setText(value);
                                            }
                                            if ("C5".equals(currentCell)) {
                                                try {
                                                    final Date valueDate = DateUtil.getJavaDate(Double.parseDouble(value));
                                                    ++currentValueIsSet;
                                                    value = new SimpleDateFormat("yyyy-MM-dd").format(valueDate);
                                                }
                                                catch (NumberFormatException ex5) {}
                                                txtPeriodePelaporan.setText(value);
                                            }
                                            if ("C7".equals(currentCell)) {
                                                ++currentValueIsSet;
                                                txtFolderSaveLocation.setText(value);
                                            }
                                            if (currentValueIsSet >= valueIsSetEndAt) {
                                                break;
                                            }
                                            MainViewController.this.endThreadOpenFile = true;
                                        }
                                    }
                                    parser.close();
                                    isSheet.close();
                                    break;
                                }
                            }
                        }
                        pkg.close();
                        if (!found) {
                            JOptionPane.showMessageDialog(null, "Sheet " + SpreadSheetUtil.UUS_SHEET_MENU_UTAMA.sheetName + " tidak ditemukan", "Informasi", 1);
                        }
                    }
                    catch (IOException ex) {
                        Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    catch (OpenXML4JException ex2) {
                        Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, (Throwable)ex2);
                    }
                    catch (XMLStreamException ex3) {
                        Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex3);
                    }
                }
            });
            t.start();
            tProccess.start();
        }
    }
    
    public void openFolderSave(final JFrame parent, final JTextField txtFolderSaveLocation) {
        if (0 == this.jfcFolder.showOpenDialog(parent)) {
            txtFolderSaveLocation.setText(this.jfcFolder.getSelectedFile().getPath());
            this.folderSave = new File(this.jfcFolder.getSelectedFile().getPath());
        }
    }
    
    public void start214021300(final JFrame parent, final JTabbedPane tabbedPane, final JLabel labelError, final JLabel labelProccessed, final JLabel labelTime, final JLabel labelTotalRow, final JProgressBar progressBar, final JComboBox<String> cmbSheet, final JTextField txtFileLocation, final JLabel labelNamaFile, final JTextField txtKodePerusahaan, final JTextField txtNamaPerusahaan, final JTextField txtPeriodePelaporan, final JTextField txtFolderSaveLocation, final JTextArea txtLog) {
        if (!this.endThreadStart) {
            JOptionPane.showMessageDialog(null, "Another proccess alredy running", "Informasi", 1);
            return;
        }
        if (this.globalR == null) {
            this.labelNotif.setForeground(Color.red);
            this.labelNotif.setText("*Please Pilih File terlebih dahulu");
            return;
        }
        if (txtFolderSaveLocation.getText() == null || txtFolderSaveLocation.getText().equals("")) {
            this.labelNotif.setForeground(Color.red);
            this.labelNotif.setText("*Please Pilih Lokasi Penyimpanan");
            return;
        }
        if (cmbSheet.getSelectedIndex() == 0) {
            this.labelNotif.setForeground(Color.red);
            this.labelNotif.setText("*Please Pilih Sheet Name");
            return;
        }
        labelError.setText("0");
        labelTime.setText("N/A");
        labelTotalRow.setText("0");
        progressBar.setValue(0);
        this.labelNotif.setText("");
        tabbedPane.setSelectedIndex(1);
        txtLog.setText("Finding sheet " + cmbSheet.getSelectedItem().toString() + "\n");
        final Thread threadTime = new Thread(new Runnable() {
            @Override
            public void run() {
                MainViewController.this.endThreadStart = false;
                final long start = System.currentTimeMillis();
                while (!MainViewController.this.endThreadStart) {
                    final long end = System.currentTimeMillis();
                    final double time = (end - start) / 1000.0;
                    final Double minute = time / 60.0;
                    final Double detik = time % 60.0;
                    final Double hour = minute / 60.0;
                    final String text = String.format("%02d", hour.intValue()) + ":" + String.format("%02d", minute.intValue()) + ":" + String.format("%02d", detik.intValue());
                    labelTime.setText(text);
                }
                MainViewController.this.endThreadStart = true;
            }
        });
        threadTime.start();
        final Thread threadProccess = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final String sheetName = cmbSheet.getSelectedItem().toString();
                    boolean found = false;
                    StringBuilder stringBuilder = new StringBuilder();
                    final SharedStringsTable sst = MainViewController.this.globalR.getSharedStringsTable();
                    final Iterator<InputStream> sheets = (Iterator<InputStream>)MainViewController.this.globalR.getSheetsData();
                    if (sheets instanceof XSSFReader.SheetIterator) {
                        final XSSFReader.SheetIterator sheetiterator = (XSSFReader.SheetIterator)sheets;
                        while (sheetiterator.hasNext()) {
                            final InputStream isSheet = sheetiterator.next();
                            System.out.println(sheetiterator.getSheetName());
                            if (sheetiterator.getSheetName().trim().toLowerCase().equals(cmbSheet.getSelectedItem().toString().toLowerCase()) && cmbSheet.getSelectedItem().toString().equals("214021300")) {
                                txtLog.append("Found sheet " + cmbSheet.getSelectedItem().toString() + "\n");
                                txtLog.append("Execute sheet " + cmbSheet.getSelectedItem().toString() + "\n");
                                found = true;
                                final XMLInputFactory fac = XMLInputFactory.newInstance();
                                final XMLStreamReader parser = fac.createXMLStreamReader(isSheet);
                                String cellType = "";
                                String currentCell = "";
                                Integer currentRow = 0;
                                Boolean isColContinue = false;
                                S214021300_TUP s214021300_tup = null;
                                Integer id = 1;
                                Integer emptyRowFound = 0;
                                Integer stringBuilderAppend = 0;
                                final String tempFileOutput = System.currentTimeMillis() + "";
                                final Integer partTempFileOutput = 0;
                                Boolean isFirstWrite = true;
                                Boolean isFirstCol = true;
                                while (parser.hasNext()) {
                                    if (MainViewController.this.endThreadStart) {
                                        break;
                                    }
                                    final int eventType = parser.next();
                                    if (eventType == 1) {
                                        if (parser.getLocalName().equals("row")) {
                                            labelTotalRow.setText(parser.getAttributeValue(0));
                                            if (s214021300_tup != null) {
                                                MainViewController.this.validateS214021300(parent, s214021300_tup, false);
                                                stringBuilder.append(s214021300_tup.buildStrXBRL());
                                                if (stringBuilderAppend >= SpreadSheetUtil.maxAppendStringBuilder) {
                                                    MainViewController.this.writeFile(txtFolderSaveLocation.getText(), txtPeriodePelaporan.getText(), txtKodePerusahaan.getText(), sheetName, stringBuilder.toString(), isFirstWrite, false, txtLog);
                                                    stringBuilder = new StringBuilder();
                                                    isFirstWrite = false;
                                                    stringBuilderAppend = 0;
                                                }
                                                else {
                                                    ++stringBuilderAppend;
                                                }
                                                labelProccessed.setText(id + "");
                                                ++id;
                                                s214021300_tup = null;
                                                isFirstCol = true;
                                            }
                                            try {
                                                currentRow = Integer.parseInt(parser.getAttributeValue(0).trim());
                                                System.out.println();
                                                System.out.print("row : " + currentRow);
                                            }
                                            catch (NumberFormatException ex5) {
                                                currentRow = null;
                                            }
                                            catch (Exception ex6) {
                                                currentRow = null;
                                            }
                                            isColContinue = false;
                                        }
                                        else {
                                            if (!parser.getLocalName().equals("c")) {
                                                continue;
                                            }
                                            currentCell = parser.getAttributeValue(0).trim();
                                            System.out.print(" | " + currentCell);
                                            cellType = parser.getAttributeValue(2);
                                            if (!isFirstCol || currentCell.equals("A" + currentRow)) {
                                                continue;
                                            }
                                            ++emptyRowFound;
                                            isFirstCol = false;
                                        }
                                    }
                                    else if (eventType == 12 | eventType == 4) {
                                        if (emptyRowFound >= SpreadSheetUtil.UUS_SHEET_214021300.maxRowBottomEmptyNumber) {
                                            break;
                                        }
                                        if (currentRow == null) {
                                            break;
                                        }
                                        if (currentRow < SpreadSheetUtil.UUS_SHEET_214021300.firstRow) {
                                            continue;
                                        }
                                        if (isColContinue) {
                                            continue;
                                        }
                                        String value = "";
                                        if (cellType != null && cellType.equals("s")) {
                                            value = parser.getText();
                                            final Integer intValue = null;
                                            try {
                                                System.out.print("kk = " + new XSSFRichTextString(sst.getEntryAt(Integer.parseInt(value))));
                                                value = String.valueOf(new XSSFRichTextString(sst.getEntryAt(Integer.parseInt(value))));
                                            }
                                            catch (NumberFormatException ex7) {
                                                System.out.print(" = " + value + "11");
                                            }
                                        }
                                        else {
                                            System.out.print(" = " + parser.getText() + "00");
                                            value = parser.getText();
                                        }
                                        System.out.println(currentCell + "as" + currentRow);
                                        if (currentCell.equals("A" + currentRow)) {
                                            System.out.print("masuk");
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            if (value == null || "".equals(value.trim())) {
                                                ++emptyRowFound;
                                                isColContinue = true;
                                                System.out.println("col continues");
                                                continue;
                                            }
                                            emptyRowFound = 0;
                                            s214021300_tup = new S214021300_TUP();
                                            s214021300_tup.setId("TUP_P_" + id);
                                            s214021300_tup.setNo(id);
                                        }
                                        if (s214021300_tup == null) {
                                            continue;
                                        }
                                        if (currentCell.equals("B" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setNoKontrak(value);
                                        }
                                        if (currentCell.equals("C" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setJenisPembiayaan(value);
                                        }
                                        System.out.println("not continue");
                                        if (currentCell.equals("D" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, true);
                                            s214021300_tup.setTanggalMulaiPembiayaan(value);
                                        }
                                        if (currentCell.equals("E" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, true);
                                            s214021300_tup.setTanggalJatuhTempo(value);
                                        }
                                        if (currentCell.equals("F" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setJenisTingkatBungaBagiHasil(value);
                                        }
                                        if (currentCell.equals("G" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, true, false);
                                            s214021300_tup.setTingkatBungaBagiHasil(value);
                                        }
                                        if (currentCell.equals("H" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setNilaiAwalPembiayaan(value);
                                        }
                                        if (currentCell.equals("I" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setKualitas(value);
                                        }
                                        if (currentCell.equals("J" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setJenisValuta(value);
                                        }
                                        if (currentCell.equals("K" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setTagihanPiutangPembiayaanBrutoDalamMataUangAsal(value);
                                        }
                                        if (currentCell.equals("L" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah(value);
                                        }
                                        if (currentCell.equals("M" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setBungaBagiHasilYangDiTangguhkanDalamMataUangAsal(value);
                                        }
                                        if (currentCell.equals("N" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah(value);
                                        }
                                        if (currentCell.equals("O" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setPiutangPembiayaanPokokDalamMataUangAsal(value);
                                        }
                                        if (currentCell.equals("P" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setPiutangPembiayaanPokokDalamEquivalenRupiah(value);
                                        }
                                        if (currentCell.equals("Q" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setMetodePembentukanCadanganKerugian(value);
                                        }
                                        if (currentCell.equals("R" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setNilaiAsetBaikCadanganKerugianPenurunanNilai(value);
                                        }
                                        if (currentCell.equals("S" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setNilaiAsetKurangBaikCadanganKerugianPenurunanNilai(value);
                                        }
                                        if (currentCell.equals("T" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setNilaiAsetTidakBaikCadanganKerugianPenurunanNilai(value);
                                        }
                                        if (currentCell.equals("U" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, true, false);
                                            s214021300_tup.setProporsiPenjaminanPembiayaan(value);
                                        }
                                        if (currentCell.equals("V" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setNamaPasanganUsahaDebitur(value);
                                        }
                                        if (currentCell.equals("W" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setBentukPasanganUsahaDebitur(value);
                                        }
                                        if (currentCell.equals("X" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setKategoriUsahaPasanganDebitur(value);
                                        }
                                        if (currentCell.equals("Y" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setKategoriUsahaKeuanganBerkelanjutan(value);
                                        }
                                        if (currentCell.equals("Z" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setGolonganPasanganUsahaDebitur(value);
                                        }
                                        if (currentCell.equals("AA" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setStatusKeterkaitan(value);
                                        }
                                        if (currentCell.equals("AB" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setLokasiKabupatenKota(value);
                                        }
                                        if (currentCell.equals("AC" + currentRow)) {
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            s214021300_tup.setSektorEknomi(value);
                                        }
                                        S214021300_TUP.F214021300_COL f214021300_col = s214021300_tup.getF214021300_col();
                                        if (currentCell.equals("AD" + currentRow)) {
                                            if (f214021300_col == null) {
                                                f214021300_col = new S214021300_TUP.F214021300_COL();
                                            }
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            f214021300_col.setId(s214021300_tup.getId() + "_1");
                                            f214021300_col.setNomorAgunan(value);
                                        }
                                        if (currentCell.equals("AE" + currentRow)) {
                                            if (f214021300_col == null) {
                                                f214021300_col = new S214021300_TUP.F214021300_COL();
                                            }
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            f214021300_col.setId(s214021300_tup.getId() + "_1");
                                            f214021300_col.setJenisAgunan(value);
                                        }
                                        if (currentCell.equals("AF" + currentRow)) {
                                            if (f214021300_col == null) {
                                                f214021300_col = new S214021300_TUP.F214021300_COL();
                                            }
                                            value = MainViewController.this.getCellValueAsStringV2(value, false, false);
                                            f214021300_col.setId(s214021300_tup.getId() + "_1");
                                            f214021300_col.setNilaiAgunan(value);
                                        }
                                        s214021300_tup.setF214021300_col(f214021300_col);
                                    }
                                    else {
                                        if (currentCell.equals("A" + currentRow) && currentRow >= SpreadSheetUtil.UUS_SHEET_214021300.firstRow) {
                                            ++emptyRowFound;
                                        }
                                        if (emptyRowFound >= SpreadSheetUtil.UUS_SHEET_214021300.maxRowBottomEmptyNumber) {
                                            break;
                                        }
                                        continue;
                                    }
                                }
                                if (s214021300_tup != null && s214021300_tup.getId() != null) {
                                    MainViewController.this.validateS214021300(parent, s214021300_tup, false);
                                    stringBuilder.append(s214021300_tup.buildStrXBRL());
                                    labelProccessed.setText(id + "");
                                }
                                parser.close();
                                isSheet.close();
                                MainViewController.this.writeFile(txtFolderSaveLocation.getText(), txtPeriodePelaporan.getText(), txtKodePerusahaan.getText(), sheetName, stringBuilder.toString(), isFirstWrite, true, txtLog);
                                break;
                            }
                        }
                    }
                    if (!found) {
                        txtLog.append("Not found sheet 214021300\n");
                        JOptionPane.showMessageDialog(null, "Sheet 214021300 tidak ditemukan", "Informasi", 1);
                    }
                    MainViewController.this.labelNotif.setText("");
                    MainViewController.this.endThreadStart = true;
                }
                catch (FileNotFoundException ex) {
                    txtLog.append("Error, " + ex.getMessage() + "\n");
                    Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, "Error " + ex.getMessage(), "Informasi", 1);
                }
                catch (IOException ex2) {
                    txtLog.append("Error, " + ex2.getMessage() + "\n");
                    Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex2);
                    JOptionPane.showMessageDialog(null, "Error " + ex2.getMessage(), "Informasi", 1);
                }
                catch (XMLStreamException ex3) {
                    txtLog.append("Error, " + ex3.getMessage() + "\n");
                    Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex3);
                    JOptionPane.showMessageDialog(null, "Error " + ex3.getMessage(), "Informasi", 1);
                }
                catch (InvalidFormatException ex4) {
                    txtLog.append("Error, " + ex4.getMessage() + "\n");
                    Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, (Throwable)ex4);
                    JOptionPane.showMessageDialog(null, "Error " + ex4.getMessage(), "Informasi", 1);
                }
            }
        });
        threadProccess.start();
    }
    
    private String getCellValueAsString(final XSSFCell cell, final Boolean isDouble) {
        String value = null;
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            if (cell.getCellType() == CellType.BOOLEAN) {
                cell.setCellType(CellType.STRING);
                value = cell.getStringCellValue();
            }
            else if (cell.getCellType() == CellType.NUMERIC) {
                cell.setCellType(CellType.STRING);
                value = cell.getStringCellValue();
            }
            else if (cell.getCellType() == CellType.STRING) {
                value = cell.getStringCellValue();
            }
            else if (DateUtil.isCellDateFormatted((Cell)cell)) {
                final Date cellValue = cell.getDateCellValue();
                if (cellValue != null) {
                    value = sdf.format(cellValue);
                }
            }
            if (value == null || value.equals("")) {
                return null;
            }
            value = value.trim();
            if (isDouble) {
                try {
                    Double.parseDouble(value);
                }
                catch (NumberFormatException e2) {
                    return value;
                }
                value = (value.contains(".") ? value : (value + ".00"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }
    
    private String getCellValueAsStringV2(String value, final Boolean isDouble, final Boolean isDate) {
        if (value == null) {
            return null;
        }
        value = value.trim();
        if (value.equals("")) {
            return null;
        }
        if (isDouble) {
            try {
                Double.parseDouble(value);
            }
            catch (NumberFormatException e) {
                return value;
            }
            value = (value.contains(".") ? value : (value + ".00"));
        }
        if (isDate) {
            try {
                final Date valueDate = DateUtil.getJavaDate(Double.parseDouble(value));
                value = new SimpleDateFormat("yyyy-MM-dd").format(valueDate);
            }
            catch (NumberFormatException ex) {}
        }
        return value;
    }
    
    private void writeFile(final String saveFolderSaveLocation, final String periodePelaporan, final String kodePerusahaan, final String sheetName, final String stringValue, final Boolean isFirstWrite, final Boolean isEnd, final JTextArea txtLog) {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                FileInputStream fis = null;
                FileOutputStream fos = null;
                try {
                    File fileXbrl = new File(saveFolderSaveLocation + "\\" + kodePerusahaan + "-" + periodePelaporan + "-" + sheetName + ".xml");
                    if (isFirstWrite) {
                        final String path214021350 = System.getProperty("user.dir") + "\\template\\214021300.xml";
                        final File template214021350 = new File(path214021350);
                        fis = new FileInputStream(template214021350);
                        final byte[] inputByte = new byte[(int)template214021350.length()];
                        fis.read(inputByte);
                        final MessageFormat mf = new MessageFormat(new String(inputByte));
                        final Calendar cal = Calendar.getInstance();
                        cal.setTime(MainViewController.this.sdf.parse(periodePelaporan));
                        cal.set(5, 1);
                        final String value = mf.format(new String[] { kodePerusahaan, MainViewController.this.sdf.format(cal.getTime()), periodePelaporan, "iso4217:IDR", stringValue });
                        fos = new FileOutputStream(fileXbrl);
                        fos.write((value + (isEnd ? "</xbrli:xbrl>" : "")).getBytes());
                        fileXbrl = null;
                    }
                    else {
                        fos = new FileOutputStream(fileXbrl, true);
                        fos.write((stringValue + (isEnd ? "</xbrli:xbrl>" : "")).getBytes());
                        fos.close();
                    }
                    if (isEnd) {
                        txtLog.append("Success createing file " + saveFolderSaveLocation + "\\" + kodePerusahaan + "-" + periodePelaporan + "-" + sheetName + ".xml\n");
                        JOptionPane.showMessageDialog(null, "Success", "Informasi", 1);
                    }
                }
                catch (FileNotFoundException ex) {
                    txtLog.append("Error, " + ex.getMessage() + "\n");
                    Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, "Error " + ex.getMessage(), "Informasi", 1);
                    MainViewController.this.endThreadStart = true;
                }
                catch (IOException ex2) {
                    txtLog.append("Error, " + ex2.getMessage() + "\n");
                    Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex2);
                    JOptionPane.showMessageDialog(null, "Error " + ex2.getMessage(), "Informasi", 1);
                    MainViewController.this.endThreadStart = true;
                }
                catch (ParseException ex3) {
                    txtLog.append("Error, " + ex3.getMessage() + "\n");
                    Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex3);
                    JOptionPane.showMessageDialog(null, "Error " + ex3.getMessage(), "Informasi", 1);
                    MainViewController.this.endThreadStart = true;
                }
                finally {
                    try {
                        fis.close();
                        fos.close();
                    }
                    catch (IOException ex4) {
                        Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex4);
                    }
                }
            }
        });
        thread.start();
    }
    
    private void validateS214021300(final JFrame parent, S214021300_TUP s214021300_tup, final Boolean castAllRowToMonetary) {
        System.out.println("validate");
        s214021300_tup.resetError();
        if (s214021300_tup.getF214021300_col() != null) {
            s214021300_tup.getF214021300_col().resetError();
        }
        boolean valid = true;
        if (Constant.isNullOrEmptyString(s214021300_tup.getNoKontrak())) {
            s214021300_tup.setErrorNoKontrak("No Kontrak wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getJenisPembiayaan())) {
            s214021300_tup.setErrorJenisPembiayaan("Jenis Pembiayaan wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getTanggalMulaiPembiayaan())) {
            s214021300_tup.setErrorTanggalMulaiPembiayaan("Tanggal Mulai Pembiyaan wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getTanggalJatuhTempo())) {
            s214021300_tup.setErrorTanggalJatuhTempo("Tanggal Jatuh Tempo wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getJenisTingkatBungaBagiHasil())) {
            s214021300_tup.setErrorJenisTingkatBungaBagiHasil("Jenis Tingkat Bunga / Bagi Hasil wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getTingkatBungaBagiHasil())) {
            s214021300_tup.setErrorTingkatBungaBagiHasil("Tingkat Bunga / Bagi Hasil wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getNilaiAwalPembiayaan())) {
            s214021300_tup.setErrorNilaiAwalPembiayaan("Nilai Awal Pembiayaan wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getKualitas())) {
            s214021300_tup.setErrorKualitas("Kualitas wajib diisi");
            valid = false;
        }
        boolean jenisValutaIsset = false;
        if (Constant.isNullOrEmptyString(s214021300_tup.getJenisValuta())) {
            s214021300_tup.setErrorJenisValuta("Jenis Valuta wajib diisi");
            jenisValutaIsset = true;
            valid = false;
        }
        if (!jenisValutaIsset && s214021300_tup.getJenisValuta().trim().equals("MU:IDR") && Constant.isNullOrEmptyString(s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamMataUangAsal())) {
            s214021300_tup.setErrorTagihanPiutangPembiayaanBrutoDalamMataUangAsal("Tagihan Piutang Pembiayaan Bruto Dalam Mata Uang Asal wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah())) {
            s214021300_tup.setErrorTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah("Tagihan Piutang Pembiayaan Bruto Dalam Equivalen Rupiah wajib diisi");
            valid = false;
        }
        if (!jenisValutaIsset && s214021300_tup.getJenisValuta().trim().equals("MU:IDR") && Constant.isNullOrEmptyString(s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamMataUangAsal())) {
            s214021300_tup.setErrorBungaBagiHasilYangDiTangguhkanDalamMataUangAsal("Bunga / Bagi Hasil Yang Di Tangguhkan Dalam Mata Uang Asal wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah())) {
            s214021300_tup.setErrorBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah("Bunga / Bagi Hasil Yang Di Tangguhkan Dalam Mata Uang Asal wajib diisi");
            valid = false;
        }
        if (!jenisValutaIsset && s214021300_tup.getJenisValuta().trim().equals("MU:IDR") && Constant.isNullOrEmptyString(s214021300_tup.getPiutangPembiayaanPokokDalamMataUangAsal())) {
            s214021300_tup.setErrorPiutangPembiayaanPokokDalamMataUangAsal("Piutang Pembiayaan Pokok Dalam Mata Uang Asal wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getPiutangPembiayaanPokokDalamEquivalenRupiah())) {
            s214021300_tup.setErrorPiutangPembiayaanPokokDalamEquivalenRupiah("Piutang Pembiayaan Pokok Dalam Equivalen Rupiah wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getMetodePembentukanCadanganKerugian())) {
            s214021300_tup.setErrorMetodePembentukanCadanganKerugian("Metode Pembentukan Cadangan Kerugian wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getNilaiAsetBaikCadanganKerugianPenurunanNilai()) && Constant.isNullOrEmptyString(s214021300_tup.getNilaiAsetKurangBaikCadanganKerugianPenurunanNilai()) && Constant.isNullOrEmptyString(s214021300_tup.getNilaiAsetTidakBaikCadanganKerugianPenurunanNilai())) {
            s214021300_tup.setErrorNilaiAsetBaikCadanganKerugianPenurunanNilai("Cadangan Kerugian Penurunan Nilai wajib diisi, Harus diisi salah satu kolom");
            s214021300_tup.setErrorNilaiAsetTidakBaikCadanganKerugianPenurunanNilai("Cadangan Kerugian Penurunan Nilai wajib diisi, Harus diisi salah satu kolom");
            s214021300_tup.setErrorNilaiAsetKurangBaikCadanganKerugianPenurunanNilai("Cadangan Kerugian Penurunan Nilai wajib diisi, Harus diisi salah satu kolom");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getProporsiPenjaminanPembiayaan())) {
            s214021300_tup.setErrorProporsiPenjaminanPembiayaan("Proporsi Penjaminan Pembiayaan wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getNamaPasanganUsahaDebitur())) {
            s214021300_tup.setErrorNamaPasanganUsahaDebitur("Nama Pasangan Usaha / Debitur wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getBentukPasanganUsahaDebitur())) {
            s214021300_tup.setErrorNamaPasanganUsahaDebitur("Bentuk Pasangan Usaha / Debitur wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getKategoriUsahaPasanganDebitur())) {
            s214021300_tup.setErrorKategoriUsahaPasanganDebitur("Kategori Usaha Pasangan / Debitur wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getKategoriUsahaKeuanganBerkelanjutan())) {
            s214021300_tup.setErrorKategoriUsahaKeuanganBerkelanjutan("Kategori Usaha Keuangan Berkelanjutan wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getGolonganPasanganUsahaDebitur())) {
            s214021300_tup.setErrorGolonganPasanganUsahaDebitur("Golongan Pasangan Usaha / Debitur wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getStatusKeterkaitan())) {
            s214021300_tup.setErrorStatusKeterkaitan("Status Keterkaitan wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getLokasiKabupatenKota())) {
            s214021300_tup.setErrorLokasiKabupatenKota("Lokasi Kabupaten Kota wajib diisi");
            valid = false;
        }
        if (Constant.isNullOrEmptyString(s214021300_tup.getSektorEknomi())) {
            s214021300_tup.setErrorSektorEknomi("Sektor Eknomi wajib diisi");
            valid = false;
        }
        if (!valid) {
            final DialogValidateS214021300 dialogValidateS214021300 = new DialogValidateS214021300(parent, true);
            dialogValidateS214021300.setS214021300_tup(s214021300_tup);
            dialogValidateS214021300.setIsValidateMonetary(false);
            dialogValidateS214021300.setVisible(true);
            s214021300_tup = dialogValidateS214021300.getS214021300_tup();
            this.validateS214021300(parent, s214021300_tup, castAllRowToMonetary);
            return;
        }
        if (!castAllRowToMonetary) {
            if (Constant.isNotMonetary(s214021300_tup.getNilaiAwalPembiayaan())) {
                s214021300_tup.setErrorNilaiAwalPembiayaan("Harap isi Nilai Awal Pembiayaan dengan type monetary");
                valid = false;
            }
            if (Constant.isNotNumberFormat(s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamMataUangAsal())) {
                s214021300_tup.setErrorTagihanPiutangPembiayaanBrutoDalamMataUangAsal("Harap isi Tagihan Piutang Pembiayaan Bruto Dalam Mata Uang Asal dengan type monetary");
                valid = false;
            }
            if (Constant.isNotMonetary(s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah())) {
                s214021300_tup.setErrorTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah("Harap isi Tagihan Piutang Pembiayaan Bruto Dalam Equivalen Rupiah dengan type monetary");
                valid = false;
            }
            if (!Constant.isNullOrEmptyString(s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamMataUangAsal()) && Constant.isNotMonetary(s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamMataUangAsal())) {
                s214021300_tup.setErrorBungaBagiHasilYangDiTangguhkanDalamMataUangAsal("Harap isi Bunga / Bagi Hasil Yang Di Tangguhkan Dalam Mata Uang Asal dengan type monetary");
                valid = false;
            }
            if (Constant.isNotMonetary(s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah())) {
                s214021300_tup.setErrorBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah("Harap isi Bunga / Bagi Hasil Yang Di Tangguhkan Dalam Mata Uang Asal dengan type monetary");
                valid = false;
            }
            if (!Constant.isNullOrEmptyString(s214021300_tup.getPiutangPembiayaanPokokDalamMataUangAsal()) && Constant.isNotMonetary(s214021300_tup.getPiutangPembiayaanPokokDalamMataUangAsal())) {
                s214021300_tup.setErrorPiutangPembiayaanPokokDalamMataUangAsal("Harap isi Piutang Pembiayaan Pokok Dalam Mata Uang Asal dengan type monetary");
                valid = false;
            }
            if (Constant.isNotMonetary(s214021300_tup.getPiutangPembiayaanPokokDalamEquivalenRupiah())) {
                s214021300_tup.setErrorPiutangPembiayaanPokokDalamEquivalenRupiah("Harap isi Piutang Pembiayaan Pokok Dalam Equivalen Rupiah dengan type monetary");
                valid = false;
            }
            if (!Constant.isNullOrEmptyString(s214021300_tup.getNilaiAsetBaikCadanganKerugianPenurunanNilai()) && Constant.isNotMonetary(s214021300_tup.getNilaiAsetBaikCadanganKerugianPenurunanNilai())) {
                s214021300_tup.setErrorNilaiAsetBaikCadanganKerugianPenurunanNilai("Harap isi Nilai Aset Baik Cadangan Kerugian Penurunan Nilai dengan type monetary");
                valid = false;
            }
            if (!Constant.isNullOrEmptyString(s214021300_tup.getNilaiAsetTidakBaikCadanganKerugianPenurunanNilai()) && Constant.isNotMonetary(s214021300_tup.getNilaiAsetTidakBaikCadanganKerugianPenurunanNilai())) {
                s214021300_tup.setErrorNilaiAsetTidakBaikCadanganKerugianPenurunanNilai("Harap isi NilaiAsetTidakBaikCadanganKerugianPenurunanNilai dengan type monetary");
                valid = false;
            }
            if (!Constant.isNullOrEmptyString(s214021300_tup.getNilaiAsetKurangBaikCadanganKerugianPenurunanNilai()) && Constant.isNotMonetary(s214021300_tup.getNilaiAsetKurangBaikCadanganKerugianPenurunanNilai())) {
                s214021300_tup.setErrorNilaiAsetKurangBaikCadanganKerugianPenurunanNilai("Harap isi Nilai Aset Kurang Baik Cadangan Kerugian Penurunan Nilai dengan type monetary");
                valid = false;
            }
            System.out.println("Valid");
            if (!valid) {
                final DialogValidateS214021300 dialogValidateS214021300 = new DialogValidateS214021300(parent, true);
                dialogValidateS214021300.setS214021300_tup(s214021300_tup);
                dialogValidateS214021300.setIsValidateMonetary(true);
                dialogValidateS214021300.setVisible(true);
                s214021300_tup = dialogValidateS214021300.getS214021300_tup();
                this.validateS214021300(parent, s214021300_tup, dialogValidateS214021300.getCastAllRowToMonetary());
                return;
            }
        }
        else {
            if (Constant.isNotNumberFormat(s214021300_tup.getNilaiAwalPembiayaan())) {
                s214021300_tup.setErrorNilaiAwalPembiayaan("Harap isi Nilai Awal Pembiayaan dengan type monetary");
                valid = false;
            }
            if (Constant.isNotNumberFormat(s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamMataUangAsal())) {
                s214021300_tup.setErrorTagihanPiutangPembiayaanBrutoDalamMataUangAsal("Harap isi Tagihan Piutang Pembiayaan Bruto Dalam Mata Uang Asal dengan type monetary");
                valid = false;
            }
            if (Constant.isNotNumberFormat(s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah())) {
                s214021300_tup.setErrorTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah("Harap isi Tagihan Piutang Pembiayaan Bruto Dalam Equivalen Rupiah dengan type monetary");
                valid = false;
            }
            if (!Constant.isNullOrEmptyString(s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamMataUangAsal()) && Constant.isNotNumberFormat(s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamMataUangAsal())) {
                s214021300_tup.setErrorBungaBagiHasilYangDiTangguhkanDalamMataUangAsal("Harap isi Bunga / Bagi Hasil Yang Di Tangguhkan Dalam Mata Uang Asal dengan type monetary");
                valid = false;
            }
            if (Constant.isNotNumberFormat(s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah())) {
                s214021300_tup.setErrorBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah("Harap isi Bunga / Bagi Hasil Yang Di Tangguhkan Dalam Mata Uang Asal dengan type monetary");
                valid = false;
            }
            if (!Constant.isNullOrEmptyString(s214021300_tup.getPiutangPembiayaanPokokDalamMataUangAsal()) && Constant.isNotNumberFormat(s214021300_tup.getPiutangPembiayaanPokokDalamMataUangAsal())) {
                s214021300_tup.setErrorPiutangPembiayaanPokokDalamMataUangAsal("Harap isi Piutang Pembiayaan Pokok Dalam Mata Uang Asal dengan type monetary");
                valid = false;
            }
            if (Constant.isNotNumberFormat(s214021300_tup.getPiutangPembiayaanPokokDalamEquivalenRupiah())) {
                s214021300_tup.setErrorPiutangPembiayaanPokokDalamEquivalenRupiah("Harap isi Piutang Pembiayaan Pokok Dalam Equivalen Rupiah dengan type monetary");
                valid = false;
            }
            if (!Constant.isNullOrEmptyString(s214021300_tup.getNilaiAsetBaikCadanganKerugianPenurunanNilai()) && Constant.isNotNumberFormat(s214021300_tup.getNilaiAsetBaikCadanganKerugianPenurunanNilai())) {
                s214021300_tup.setErrorNilaiAsetBaikCadanganKerugianPenurunanNilai("Harap isi Nilai Aset Baik Cadangan Kerugian Penurunan Nilai dengan type monetary");
                valid = false;
            }
            if (!Constant.isNullOrEmptyString(s214021300_tup.getNilaiAsetTidakBaikCadanganKerugianPenurunanNilai()) && Constant.isNotNumberFormat(s214021300_tup.getNilaiAsetTidakBaikCadanganKerugianPenurunanNilai())) {
                s214021300_tup.setErrorNilaiAsetTidakBaikCadanganKerugianPenurunanNilai("Harap isi NilaiAsetTidakBaikCadanganKerugianPenurunanNilai dengan type monetary");
                valid = false;
            }
            if (!Constant.isNullOrEmptyString(s214021300_tup.getNilaiAsetKurangBaikCadanganKerugianPenurunanNilai()) && Constant.isNotNumberFormat(s214021300_tup.getNilaiAsetKurangBaikCadanganKerugianPenurunanNilai())) {
                s214021300_tup.setErrorNilaiAsetKurangBaikCadanganKerugianPenurunanNilai("Harap isi Nilai Aset Kurang Baik Cadangan Kerugian Penurunan Nilai dengan type monetary");
                valid = false;
            }
            if (!valid) {
                final DialogValidateS214021300 dialogValidateS214021300 = new DialogValidateS214021300(parent, true);
                dialogValidateS214021300.setS214021300_tup(s214021300_tup);
                dialogValidateS214021300.setIsValidateMonetary(false);
                dialogValidateS214021300.setVisible(true);
                s214021300_tup = dialogValidateS214021300.getS214021300_tup();
                this.validateS214021300(parent, s214021300_tup, castAllRowToMonetary);
                return;
            }
            s214021300_tup.setNilaiAwalPembiayaan((s214021300_tup.getNilaiAwalPembiayaan().split(".").length > 0) ? s214021300_tup.getNilaiAwalPembiayaan().split(".")[0] : s214021300_tup.getNilaiAwalPembiayaan());
            s214021300_tup.setTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah((s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah().split(".").length > 0) ? s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah().split(".")[0] : s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah());
            s214021300_tup.setTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah((s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah().split(".").length > 0) ? s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah().split(".")[0] : s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah());
            s214021300_tup.setBungaBagiHasilYangDiTangguhkanDalamMataUangAsal((s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamMataUangAsal().split(".").length > 0) ? s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamMataUangAsal().split(".")[0] : s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamMataUangAsal());
            s214021300_tup.setBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah((s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah().split(".").length > 0) ? s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah().split(".")[0] : s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah());
            s214021300_tup.setPiutangPembiayaanPokokDalamMataUangAsal((s214021300_tup.getPiutangPembiayaanPokokDalamMataUangAsal().split(".").length > 0) ? s214021300_tup.getPiutangPembiayaanPokokDalamMataUangAsal().split(".")[0] : s214021300_tup.getPiutangPembiayaanPokokDalamMataUangAsal());
            s214021300_tup.setPiutangPembiayaanPokokDalamEquivalenRupiah((s214021300_tup.getPiutangPembiayaanPokokDalamEquivalenRupiah().split(".").length > 0) ? s214021300_tup.getPiutangPembiayaanPokokDalamEquivalenRupiah().split(".")[0] : s214021300_tup.getPiutangPembiayaanPokokDalamEquivalenRupiah());
            s214021300_tup.setNilaiAsetBaikCadanganKerugianPenurunanNilai((s214021300_tup.getNilaiAsetBaikCadanganKerugianPenurunanNilai().split(".").length > 0) ? s214021300_tup.getNilaiAsetBaikCadanganKerugianPenurunanNilai().split(".")[0] : s214021300_tup.getNilaiAsetBaikCadanganKerugianPenurunanNilai());
            s214021300_tup.setNilaiAsetTidakBaikCadanganKerugianPenurunanNilai((s214021300_tup.getNilaiAsetTidakBaikCadanganKerugianPenurunanNilai().split(".").length > 0) ? s214021300_tup.getNilaiAsetTidakBaikCadanganKerugianPenurunanNilai().split(".")[0] : s214021300_tup.getNilaiAsetTidakBaikCadanganKerugianPenurunanNilai());
            s214021300_tup.setNilaiAsetKurangBaikCadanganKerugianPenurunanNilai((s214021300_tup.getNilaiAsetKurangBaikCadanganKerugianPenurunanNilai().split(".").length > 0) ? s214021300_tup.getNilaiAsetKurangBaikCadanganKerugianPenurunanNilai().split(".")[0] : s214021300_tup.getNilaiAsetKurangBaikCadanganKerugianPenurunanNilai());
        }
        if (Constant.isNonAscii(s214021300_tup.getNoKontrak())) {
            s214021300_tup.setErrorNoKontrak("No Kontrak contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getJenisPembiayaan())) {
            s214021300_tup.setErrorJenisPembiayaan("Jenis Pembiayaan contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getTanggalMulaiPembiayaan())) {
            s214021300_tup.setErrorTanggalMulaiPembiayaan("Tanggal Mulai Pembiyaan contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getTanggalJatuhTempo())) {
            s214021300_tup.setErrorTanggalJatuhTempo("Tanggal Jatuh Tempo contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getJenisTingkatBungaBagiHasil())) {
            s214021300_tup.setErrorJenisTingkatBungaBagiHasil("Jenis Tingkat Bunga / Bagi Hasil contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getTingkatBungaBagiHasil())) {
            s214021300_tup.setErrorTingkatBungaBagiHasil("Tingkat Bunga / Bagi Hasil contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getNilaiAwalPembiayaan())) {
            s214021300_tup.setErrorNilaiAwalPembiayaan("Nilai Awal Pembiayaan contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getKualitas())) {
            s214021300_tup.setErrorKualitas("Kualitas contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getJenisValuta())) {
            s214021300_tup.setErrorJenisValuta("Jenis Valuta contains an invalid character");
            valid = false;
        }
        if (!Constant.isNullOrEmptyString(s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamMataUangAsal()) && Constant.isNonAscii(s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamMataUangAsal())) {
            s214021300_tup.setErrorTagihanPiutangPembiayaanBrutoDalamMataUangAsal("Tagihan Piutang Pembiayaan Bruto Dalam Mata Uang Asal contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah())) {
            s214021300_tup.setErrorTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah("Tagihan Piutang Pembiayaan Bruto Dalam Equivalen Rupiah contains an invalid character");
            valid = false;
        }
        if (!Constant.isNullOrEmptyString(s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamMataUangAsal()) && Constant.isNonAscii(s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamMataUangAsal())) {
            s214021300_tup.setErrorBungaBagiHasilYangDiTangguhkanDalamMataUangAsal("Bunga / Bagi Hasil Yang Di Tangguhkan Dalam Mata Uang Asal contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah())) {
            s214021300_tup.setErrorBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah("Bunga / Bagi Hasil Yang Di Tangguhkan Dalam Mata Uang Asal contains an invalid character");
            valid = false;
        }
        if (!Constant.isNullOrEmptyString(s214021300_tup.getPiutangPembiayaanPokokDalamMataUangAsal()) && Constant.isNonAscii(s214021300_tup.getPiutangPembiayaanPokokDalamMataUangAsal())) {
            s214021300_tup.setErrorPiutangPembiayaanPokokDalamMataUangAsal("Piutang Pembiayaan Pokok Dalam Mata Uang Asal contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getPiutangPembiayaanPokokDalamEquivalenRupiah())) {
            s214021300_tup.setErrorPiutangPembiayaanPokokDalamEquivalenRupiah("Piutang Pembiayaan Pokok Dalam Equivalen Rupiah contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getMetodePembentukanCadanganKerugian())) {
            s214021300_tup.setErrorMetodePembentukanCadanganKerugian("Metode Pembentukan Cadangan Kerugian contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getNilaiAsetBaikCadanganKerugianPenurunanNilai())) {
            s214021300_tup.setErrorNilaiAsetBaikCadanganKerugianPenurunanNilai("Cadangan Kerugian Penurunan Nilai contains an invalid character, Harus diisi salah satu kolom");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getNilaiAsetKurangBaikCadanganKerugianPenurunanNilai())) {
            s214021300_tup.setErrorNilaiAsetKurangBaikCadanganKerugianPenurunanNilai("Cadangan Kerugian Penurunan Nilai contains an invalid character, Harus diisi salah satu kolom");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getNilaiAsetTidakBaikCadanganKerugianPenurunanNilai())) {
            s214021300_tup.setErrorNilaiAsetTidakBaikCadanganKerugianPenurunanNilai("Cadangan Kerugian Penurunan Nilai contains an invalid character, Harus diisi salah satu kolom");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getProporsiPenjaminanPembiayaan())) {
            s214021300_tup.setErrorProporsiPenjaminanPembiayaan("Proporsi Penjaminan Pembiayaan contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getNamaPasanganUsahaDebitur())) {
            s214021300_tup.setErrorNamaPasanganUsahaDebitur("Nama Pasangan Usaha / Debitur contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getBentukPasanganUsahaDebitur())) {
            s214021300_tup.setErrorNamaPasanganUsahaDebitur("Bentuk Pasangan Usaha / Debitur contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getKategoriUsahaPasanganDebitur())) {
            s214021300_tup.setErrorKategoriUsahaPasanganDebitur("Kategori Usaha Pasangan / Debitur contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getKategoriUsahaKeuanganBerkelanjutan())) {
            s214021300_tup.setErrorKategoriUsahaKeuanganBerkelanjutan("Kategori Usaha Keuangan Berkelanjutan contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getGolonganPasanganUsahaDebitur())) {
            s214021300_tup.setErrorGolonganPasanganUsahaDebitur("Golongan Pasangan Usaha / Debitur contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getStatusKeterkaitan())) {
            s214021300_tup.setErrorStatusKeterkaitan("Status Keterkaitan contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getLokasiKabupatenKota())) {
            s214021300_tup.setErrorLokasiKabupatenKota("Lokasi Kabupaten Kota contains an invalid character");
            valid = false;
        }
        if (Constant.isNonAscii(s214021300_tup.getSektorEknomi())) {
            s214021300_tup.setErrorSektorEknomi("Sektor Eknomi contains an invalid character");
            valid = false;
        }
        if (!valid) {
            final DialogValidateS214021300 dialogValidateS214021300 = new DialogValidateS214021300(parent, true);
            dialogValidateS214021300.setS214021300_tup(s214021300_tup);
            dialogValidateS214021300.setIsValidateMonetary(false);
            dialogValidateS214021300.setVisible(true);
            s214021300_tup = dialogValidateS214021300.getS214021300_tup();
            this.validateS214021300(parent, s214021300_tup, castAllRowToMonetary);
            return;
        }
        if (Constant.isNotDecimal(s214021300_tup.getTingkatBungaBagiHasil())) {
            s214021300_tup.setErrorTingkatBungaBagiHasil("Harap isi Tingkat Bunga / Bagi Hasil dengan type decimal");
            valid = false;
        }
        if (Constant.isNotDecimal(s214021300_tup.getProporsiPenjaminanPembiayaan())) {
            s214021300_tup.setErrorProporsiPenjaminanPembiayaan("Harap isi Proporsi Penjaminan Pembiayaan dengan type decimal");
            valid = false;
        }
        if (!valid) {
            final DialogValidateS214021300 dialogValidateS214021300 = new DialogValidateS214021300(parent, true);
            dialogValidateS214021300.setS214021300_tup(s214021300_tup);
            dialogValidateS214021300.setIsValidateMonetary(false);
            dialogValidateS214021300.setVisible(true);
            s214021300_tup = dialogValidateS214021300.getS214021300_tup();
            this.validateS214021300(parent, s214021300_tup, castAllRowToMonetary);
            return;
        }
        if (Constant.isNotDateYyyyMMDD(s214021300_tup.getTanggalMulaiPembiayaan())) {
            s214021300_tup.setErrorTanggalMulaiPembiayaan("Harap isi Tanggal Mulai Pembiyaan dengan tipe date");
            valid = false;
        }
        if (Constant.isNotDateYyyyMMDD(s214021300_tup.getTanggalJatuhTempo())) {
            s214021300_tup.setErrorTanggalJatuhTempo("Harap isi Tanggal Jatuh Tempo dengan tipe date");
            valid = false;
        }
        if (!valid) {
            final DialogValidateS214021300 dialogValidateS214021300 = new DialogValidateS214021300(parent, true);
            dialogValidateS214021300.setS214021300_tup(s214021300_tup);
            dialogValidateS214021300.setIsValidateMonetary(false);
            dialogValidateS214021300.setVisible(true);
            s214021300_tup = dialogValidateS214021300.getS214021300_tup();
            this.validateS214021300(parent, s214021300_tup, castAllRowToMonetary);
            return;
        }
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            final Date dateTanggalMulaiPembiayaan = sdf.parse(s214021300_tup.getTanggalMulaiPembiayaan());
            final Date dateTanggalJatuhTempo = sdf.parse(s214021300_tup.getTanggalJatuhTempo());
            if (dateTanggalMulaiPembiayaan.after(dateTanggalJatuhTempo)) {
                s214021300_tup.setErrorTanggalMulaiPembiayaan("Tanggal Mulai Pembiyaan tidak boleh lebih dari Tanggal Jatuh Tempo");
                valid = false;
            }
        }
        catch (ParseException ex) {}
        if (!valid) {
            final DialogValidateS214021300 dialogValidateS214021300 = new DialogValidateS214021300(parent, true);
            dialogValidateS214021300.setS214021300_tup(s214021300_tup);
            dialogValidateS214021300.setIsValidateMonetary(false);
            dialogValidateS214021300.setVisible(true);
            s214021300_tup = dialogValidateS214021300.getS214021300_tup();
            this.validateS214021300(parent, s214021300_tup, castAllRowToMonetary);
        }
    }
}
