// 
// Decompiled by Procyon v0.5.36
// 

package util;

public class SpreadSheetUtil
{
    public static final Integer maxAppendStringBuilder;
    public static final String ROW = "row";
    public static final String CELL = "c";
    public static final String CELL_TYPE = "t";
    public static final String CELL_TYPE_STRING = "s";
    public static final String JAVA_HEAP_SPACE = "";
    
    static {
        maxAppendStringBuilder = 2000;
    }
    
    public static class UUS_SHEET_214021350
    {
        public static final Integer maxRowBottomEmptyNumber;
        public static final String sheetName = "214021350";
        public static final Integer firstRow;
        public static final String COL_no = "A";
        public static final String COL_noKontrak = "B";
        public static final String COL_tanggalMulaiPembiayaanIsDate = "C";
        public static final String COL_tanggalJatuhTempoIsDate = "D";
        public static final String COL_jenisBagiHasil = "E";
        public static final String COL_tingkatBagiHasilIsDouble = "F";
        public static final String COL_nilaiAwalPembiayaan = "G";
        public static final String COL_kualitas = "H";
        public static final String COL_jenisValuta = "I";
        public static final String COL_tagihanPiutangPembiayaanBrutoDalamMataUangAsal = "J";
        public static final String COL_tagihanPiutangPembiayaanBrutoDalamEquivalenRupiah = "K";
        public static final String COL_bungaBagiHasilYangDiTangguhkanDalamMataUangAsal = "L";
        public static final String COL_bungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah = "M";
        public static final String COL_piutangPembiayaanPokokDalamMataUangAsal = "N";
        public static final String COL_piutangPembiayaanPokokDalamEquivalenRupiah = "O";
        public static final String COL_metodePembentukanCadanganKerugian = "P";
        public static final String COL_nilaiAsetBaikCadanganKerugianPenurunanNilai = "Q";
        public static final String COL_nilaiAsetKurangBaikCadanganKerugianPenurunanNilai = "R";
        public static final String COL_nilaiAsetTidakBaikCadanganKerugianPenurunanNilai = "S";
        public static final String COL_proporsiPenjaminanPembiayaanIsDouble = "T";
        public static final String COL_namaPasanganUsahaDebitur = "U";
        public static final String COL_bentukPasanganUsahaDebitur = "V";
        public static final String COL_kategoriUsahaPasanganDebitur = "W";
        public static final String COL_kategoriUsahaKeuanganBerkelanjutan = "X";
        public static final String COL_golonganPasanganUsahaDebitur = "Y";
        public static final String COL_statusKeterkaitan = "Z";
        public static final String COL_lokasiKabupatenKota = "AA";
        public static final String COL_sektorEknomi = "AB";
        public static final String COL_nomorAgunan = "AC";
        public static final String COL_jenisAgunan = "AD";
        public static final String COL_nilaiAgunan = "AE";
        
        static {
            maxRowBottomEmptyNumber = 5;
            firstRow = 5;
        }
    }
    
    public static class UUS_SHEET_214021300
    {
        public static final Integer maxRowBottomEmptyNumber;
        public static final String sheetName = "214021300";
        public static final Integer firstRow;
        public static final String COL_no = "A";
        public static final String COL_noKontrak = "B";
        public static final String COL_jenisPembiayaan = "C";
        public static final String COL_tanggalMulaiPembiayaanIsDate = "D";
        public static final String COL_tanggalJatuhTempoIsDate = "E";
        public static final String COL_jenisTingkatBungaBagiHasil = "F";
        public static final String COL_tingkatBungaBagiHasilIsDouble = "G";
        public static final String COL_nilaiAwalPembiayaan = "H";
        public static final String COL_kualitas = "I";
        public static final String COL_jenisValuta = "J";
        public static final String COL_tagihanPiutangPembiayaanBrutoDalamMataUangAsal = "K";
        public static final String COL_tagihanPiutangPembiayaanBrutoDalamEquivalenRupiah = "L";
        public static final String COL_bungaBagiHasilYangDiTangguhkanDalamMataUangAsal = "M";
        public static final String COL_bungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah = "N";
        public static final String COL_piutangPembiayaanPokokDalamMataUangAsal = "O";
        public static final String COL_piutangPembiayaanPokokDalamEquivalenRupiah = "P";
        public static final String COL_metodePembentukanCadanganKerugian = "Q";
        public static final String COL_nilaiAsetBaikCadanganKerugianPenurunanNilai = "R";
        public static final String COL_nilaiAsetKurangBaikCadanganKerugianPenurunanNilai = "S";
        public static final String COL_nilaiAsetTidakBaikCadanganKerugianPenurunanNilai = "T";
        public static final String COL_proporsiPenjaminanPembiayaanIsDouble = "U";
        public static final String COL_namaPasanganUsahaDebitur = "V";
        public static final String COL_bentukPasanganUsahaDebitur = "W";
        public static final String COL_kategoriUsahaPasanganDebitur = "X";
        public static final String COL_kategoriUsahaKeuanganBerkelanjutan = "Y";
        public static final String COL_golonganPasanganUsahaDebitur = "Z";
        public static final String COL_statusKeterkaitan = "AA";
        public static final String COL_lokasiKabupatenKota = "AB";
        public static final String COL_sektorEknomi = "AC";
        public static final String COL_nomorAgunan = "AD";
        public static final String COL_jenisAgunan = "AE";
        public static final String COL_nilaiAgunan = "AF";
        
        static {
            maxRowBottomEmptyNumber = 5;
            firstRow = 5;
        }
    }
    
    public static class UUS_SHEET_MENU_UTAMA
    {
        public static final String pKodePerusahaan = "C3";
        public static final String pNamaPerusahaan = "C4";
        public static final String pPeriodePelaporan = "C5";
        public static final String pLokasiPenyimpananInstance = "C7";
        public static String sheetName;
        
        static {
            UUS_SHEET_MENU_UTAMA.sheetName = "Menu Utama";
        }
    }
}
