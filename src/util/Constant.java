// 
// Decompiled by Procyon v0.5.36
// 

package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import com.google.common.base.CharMatcher;

public class Constant
{
    public static final String[] listSheetNameAvailible;
    
    public static boolean isNullOrEmptyString(final String value) {
        return value == null || value.equals("");
    }
    
    public static boolean isNotMonetary(final String value) {
        if (value.contains(".")) {
            return true;
        }
        try {
            Integer.parseInt(value);
            return false;
        }
        catch (NumberFormatException ex) {
            return true;
        }
    }
    
    public static boolean isNotNumberFormat(final String value) {
        try {
            Integer.parseInt(value);
            return false;
        }
        catch (NumberFormatException ex) {
            try {
                Double.parseDouble(value);
                return false;
            }
            catch (NumberFormatException ex2) {
                return true;
            }
        }
    }
    
    public static boolean isNotDecimal(final String value) {
        if (!value.contains(".")) {
            return true;
        }
        try {
            Double.parseDouble(value);
            return false;
        }
        catch (NumberFormatException ex) {
            return true;
        }
    }
    
    public static boolean isNonAscii(final String value) {
        if (value == null) {
            return false;
        }
        final boolean isAscii = CharMatcher.ascii().matchesAllOf((CharSequence)value);
        return !isAscii;
    }
    
    public static boolean isNotDateYyyyMMDD(final String value) {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.parse(value);
            return false;
        }
        catch (ParseException e) {
            return true;
        }
    }
    
    static {
        listSheetNameAvailible = new String[] { "214021300" };
    }
}
